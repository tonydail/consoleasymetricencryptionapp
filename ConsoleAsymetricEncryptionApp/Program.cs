﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAsymetricEncryptionApp
{
    class Program
    {
        static void Main(string[] args)
        {

            const int keySize = 2048;
            string publicAndPrivateKey;
            string publicKey;

            EncryptionHelper.GenerateKeys(keySize, out publicKey, out publicAndPrivateKey);


            string profile = "111731.181DD3C69730906EA597F268C80E10E7589793E9.drphil.iPhone9,1.E12:D2:F23:G45";


            while (true)
            {
                Console.WriteLine("************** Start *********************");
                Console.WriteLine("Profile: {0}", profile);

                var fingerPrint = FingerprintToken.GenerateFromDeviceProfile(profile);
                string plainTextFingerPrint = fingerPrint.ToString();
                string encodedFingerprint = fingerPrint.ToEncodedString();
                string encryptedFingerprint = EncryptionHelper.EncryptText(encodedFingerprint, keySize, publicKey);
                string decryptedFingerprint = EncryptionHelper.DecryptText(encryptedFingerprint, keySize, publicAndPrivateKey);

                Console.WriteLine("Plain Text Fingerprint: {0}", plainTextFingerPrint);
                Console.WriteLine("Encoded Fingerprint: {0}", encodedFingerprint);
                Console.WriteLine("Encrypted: {0}", encryptedFingerprint);
                Console.WriteLine("Decrypted: {0}", decryptedFingerprint);

                var newFingerPrint = FingerprintToken.FromEncodedToken(decryptedFingerprint);

                Console.WriteLine("From decrypted token: {0}", newFingerPrint.ToString());
                Console.WriteLine("Is Valid Against Profile: {0}", newFingerPrint.IsValidAgainstProfile(profile));
                Console.Read();
            }

            

        }
    }
}
