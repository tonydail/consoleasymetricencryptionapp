﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAsymetricEncryptionApp
{
        public class FingerprintToken
        {
            private const char TOKEN_DELIMITER= '.';
            private const int MIN_TOKEN_COMPONENT_COUNT = 3;
            private const int MIN_DEVICE_PROFILE_COMPONENTS_COUNT = 3;

            public string FingerPrint;
            public DateTime Date;
            public string PartSequence;

            public string ToEncodedString()
            {
                var tokenParts = new []{ EncodeString(FingerPrint), EncodeString(Date.ToShortDateString()), EncodeString(PartSequence) };
                return string.Join(Convert.ToString(TOKEN_DELIMITER), tokenParts);
            }

            public override string ToString()
            {
                var tokenParts = new[] { FingerPrint, Date.ToShortDateString(), PartSequence };
                return string.Join(Convert.ToString(TOKEN_DELIMITER), tokenParts);
            }

            public static FingerprintToken FromEncodedToken( string encodedToken)
            {
                var token = new FingerprintToken();
                var parts = encodedToken.Split(TOKEN_DELIMITER);
                if(parts.Length != MIN_TOKEN_COMPONENT_COUNT)
                {
                    throw new ArgumentException("Not enough components in Encoded Token", nameof(encodedToken));
                }
                else
                {
                    token.FingerPrint = DecodeString(parts[0]);
                    token.Date = Convert.ToDateTime(DecodeString(parts[1]));
                    token.PartSequence = DecodeString(parts[2]);
                }

                return token;
            }
            

            public bool IsValidAgainstProfile( string deviceProfile)
            {

                if(string.IsNullOrEmpty(deviceProfile) || string.IsNullOrWhiteSpace(deviceProfile))
                {
                    throw new ArgumentException("Device Profile cannot be empty.", nameof(deviceProfile));
                }

                var FingerPrintParts = FingerPrint.Split(TOKEN_DELIMITER);
                var ProfileParts = deviceProfile.Split(TOKEN_DELIMITER);
                var PartSequenceParts = PartSequence.Split(TOKEN_DELIMITER);

                if(FingerPrintParts.Length == PartSequenceParts.Length)
                {
                    for(var i=0;i<=FingerPrintParts.Length-1; i++)
                    {
                        if(FingerPrintParts[i] != ProfileParts[Convert.ToInt32(PartSequenceParts[i])])
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
                return true;
            }

            public static FingerprintToken GenerateFromDeviceProfile(string deviceProfile)
            {

                if (string.IsNullOrEmpty(deviceProfile) || string.IsNullOrWhiteSpace(deviceProfile))
                {
                    throw new ArgumentException("Device Profile cannot be empty.", nameof(deviceProfile));
                }


                var profileParts = deviceProfile.Split(TOKEN_DELIMITER);

                if (profileParts.Length < MIN_DEVICE_PROFILE_COMPONENTS_COUNT)
                {
                    throw new ArgumentException("Not enough components in Device Profile", nameof(deviceProfile));
                }

                var partsList = new List<string>();
                var partNumbers = new List<string>();

                var random = new Random(DateTime.UtcNow.Millisecond * 4);

                    for (var i = 0; i <= MIN_DEVICE_PROFILE_COMPONENTS_COUNT - 1; i++)
                    {

                        int partNumber = random.Next(0, profileParts.Length);
                        partsList.Add(profileParts[partNumber]);
                        partNumbers.Add(partNumber.ToString());

                    }

                    FingerprintToken token = new FingerprintToken();
                    token.FingerPrint = string.Join(Convert.ToString(TOKEN_DELIMITER), partsList.ToArray());
                    token.PartSequence = string.Join(Convert.ToString(TOKEN_DELIMITER), partNumbers.ToArray());
                    token.Date = DateTime.UtcNow;

                    return token;
            }


            public static string EncodeString(string plainString)
            {
                byte[] stringAsBytes = new System.Text.UTF8Encoding().GetBytes(plainString);
                return Convert.ToBase64String(stringAsBytes);
            }

            public  static  string DecodeString(string encodedString)
            {
                var base64EncodedBytes = System.Convert.FromBase64String(encodedString);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
        }
}
